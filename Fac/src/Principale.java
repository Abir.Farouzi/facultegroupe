public class Principale {

	public static void main(String[] args) {
		Etudiant e1 = new Etudiant("riri",12.1,13.3);
		Etudiant e2 = new Etudiant("fifi",12.1,6);
		Etudiant e3 = new Etudiant("loulou",6.5,4);
		
		Etudiant[] tab = {e1,e2,e3};
		Classe c = new Classe(tab);
		c.affiche();

	}
}
