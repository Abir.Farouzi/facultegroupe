
public class Etudiant {
	String nom; // nom de l'�tudiant
	double s1; // note semestre 1
	double s2; // note semestre 1
	
	public Etudiant(String n, double n1, double n2)
	{
		nom = n;
		s1 = n1;
		s2 = n2;
	}
	
	public int passage()
	{
		int resu = 2; // passage par d�faut
		if (s1<0 || s2 <0) // ABI
			resu = 0; //  redoublement
		else{
		 double moyenne = (s1+s2)/2; 
		 if (moyenne <10) // pas la moyenne
		 	if (s1>10 || s2>10 || moyenne > 8)
		 		resu = 1; //rattrapage
		 	else
		 		resu = 0;  // redoublement
		}
		return resu;
	}

	public void affiche()
	{
		System.out.print("nom "+nom+" note semestre 1 "+ s1+" note de semestre 2 "+s2+" l'�tudiant ");
		int resu = passage();
		if (resu==2)
		System.out.println("passe");
		if (resu==1)
		System.out.println("est en deuxi�me session");
		if (resu==0)
		System.out.println("redouble");
	}
}
